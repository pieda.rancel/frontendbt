import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function FormDialog({open, setOpen, setCategories}) {
  const [name, setName] = useState('')

  const handleClose = () => {
    setOpen(false);
  };

  const handleInput = (e) => {
    console.log(e.target.value)
    setName(e.target.value)
  }

  const submitCategory = (e) => {
    e.preventDefault()
    const payload = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name
      })
    }

    fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories/`, payload)
    .then(res => res.json())
    .then(data => {
      if(data.length > 0){
        setCategories(data)
        setOpen(false)
      }
    })
  }



  return (
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title" className="text-center">Create Category</DialogTitle>
        <form onSubmit={submitCategory}>
        <DialogContent>
        <DialogContentText>
            Add a new category by specifying a name
        </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Category Name"
            fullWidth
            onChange={handleInput}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button color="primary" type="submit">
            Submit
          </Button>
        </DialogActions>
        </form>
      </Dialog>
  );
}