import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundImage: "linear-gradient(90deg, rgba(222,218,218,0.5575805322128851) 0%, rgba(193,187,187,0.5763480392156863) 100%)"
  },
}));

export default function DividerMaterial({
  setAllActiveClicked, 
  recordNames, 
  setLengthsClicked, 
  setAmountsClicked,
  setAllActiveExpensesClicked,
  setAllActiveIncomesClicked,
  lengthsClicked,
  amountsClicked,
  allActiveClicked,
  allActiveIncomesClicked,
  allActiveExpensesClicked
}) {

  const classes = useStyles();

  const handleLengthsClicked = () => {
    setLengthsClicked(true)
    setAmountsClicked(false)
    setAllActiveClicked(false)
    setAllActiveExpensesClicked(false)
    setAllActiveIncomesClicked(false)
  }

  const handleAmountsClicked = () => {
    setLengthsClicked(false)
    setAmountsClicked(true)
    setAllActiveClicked(false)
    setAllActiveExpensesClicked(false)
    setAllActiveIncomesClicked(false)
  }

  const handleInputClicked = () => {
    setLengthsClicked(false)
    setAmountsClicked(false)
    setAllActiveClicked(true)
    setAllActiveExpensesClicked(false)
    setAllActiveIncomesClicked(false)
  }

  const handleIncomesClicked = () => {
    setLengthsClicked(false)
    setAmountsClicked(false)
    setAllActiveClicked(false)
    setAllActiveExpensesClicked(false)
    setAllActiveIncomesClicked(true)   
  }

    const handleExpensesClicked = () => {
    setLengthsClicked(false)
    setAmountsClicked(false)
    setAllActiveClicked(false)
    setAllActiveExpensesClicked(true)
    setAllActiveIncomesClicked(false)   
  }

  return (
    <>
    <List component="nav" className={classes.root} aria-label="mailbox folders">
      <ListItem button onClick={handleLengthsClicked} selected = {lengthsClicked}>
        <ListItemText primary="Record Counter" className="text-center" />
      </ListItem>
      <Divider />
      <ListItem button onClick = {handleAmountsClicked} selected = {amountsClicked}>
        <ListItemText primary="Total Income vs. Total Expense" className="text-center" />
      </ListItem>
      <Divider />
      <ListItem button onClick = {handleInputClicked} selected = {allActiveClicked}>
        <ListItemText primary="All Records" className="text-center" />
      </ListItem>
      <Divider />
      <ListItem button onClick = {handleIncomesClicked} selected = {allActiveIncomesClicked}>
        <ListItemText primary="Incomes Overview" className="text-center" />
      </ListItem>
      <Divider />
      <ListItem button onClick = {handleExpensesClicked} selected = {allActiveExpensesClicked}>
        <ListItemText primary="Expenses Overview" className="text-center" />
      </ListItem>
    </List>
    </>
  );
}