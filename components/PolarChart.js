import React from 'react'
import {Polar} from 'react-chartjs-2'


export default function PolarChart({
	records, 
	recordNames, 
	activeExpenses,
	activeExpensesNames,
	activeIncomes,
	activeIncomesNames,
	iRLengths,
	eRLengths,
	iRAmounts,
	eRAmounts,
	lengthsClicked,
	amountsClicked,
	allActiveClicked,
	allActiveIncomesClicked,
	allActiveExpensesClicked,
}) {


console.log(activeIncomes,"activeIncomes")
console.log(activeIncomesNames, "activeIncomesNames")
console.log(activeExpenses, "activeExpenses")
console.log(activeExpensesNames, "activeExpensesNames")
console.log(allActiveClicked,
	allActiveIncomesClicked,
	allActiveExpensesClicked)

const listOfColors = [
  "Aqua",
  "BlueViolet",
  "Brown",
  "CadetBlue",
  "Chartreuse",
  "Coral",
  "CornflowerBlue",
  "Cornsilk",
  "Crimson",
  "Cyan",
  "DarkBlue",
  "DarkCyan",
  "DarkGoldenRod",
  "DarkGray",
  "DarkGrey",
  "DarkGreen",
  "DarkKhaki",
  "DarkMagenta",
  "DarkOliveGreen",
  "DarkOrange",
  "DarkOrchid",
  "DarkRed",
  "DarkSalmon",
  "DarkSeaGreen",
  "DarkSlateBlue",
  "DarkSlateGray",
  "DarkSlateGrey",
  "DarkTurquoise",
  "DarkViolet",
  "DeepPink",
  "DeepSkyBlue",
  "DimGray",
  "DimGrey",
  "DodgerBlue",
  "FireBrick",
  "FloralWhite",
  "ForestGreen",
  "Fuchsia",
  "Gainsboro",
  "GhostWhite",
  "Gold",
  "GoldenRod",
  "Gray",
  "Grey",
  "Green",
  "GreenYellow",
  "HoneyDew",
  "HotPink",
  "IndianRed",
  "Indigo",
  "Ivory",
  "Khaki",
  "Lavender",
  "LavenderBlush",
  "LawnGreen",
  "LemonChiffon",
  "LightBlue",
  "LightCoral",
  "Lime",
  "LimeGreen",
  "Linen",
  "Magenta",
  "Maroon",
  "MediumAquaMarine",
  "MediumBlue",
  "MediumOrchid",
  "MediumPurple",
  "MediumSeaGreen",
  "MediumSlateBlue",
  "MediumSpringGreen",
  "MediumTurquoise",
  "MediumVioletRed",
  "MidnightBlue",
  "MintCream",
  "MistyRose",
  "Moccasin",
  "NavajoWhite",
  "Navy",
  "OldLace",
  "Olive",
  "OliveDrab",
  "Orange",
  "OrangeRed",
]


	return (
		<>
		<Polar
		data={{
			datasets: [{
				data: lengthsClicked ? [iRLengths, eRLengths]
									: amountsClicked ? [iRAmounts, eRAmounts]
									: allActiveClicked ? records
									: allActiveExpensesClicked ? activeExpenses
									: allActiveIncomesClicked ? activeIncomes
									: [""] ,
				backgroundColor: listOfColors
			}],
			labels: 
			lengthsClicked ? ["Number of Income entries","Number of Expense Entries"]
							: amountsClicked ? ["Total Income","Total Expenses"]
							: allActiveClicked ? recordNames
							: allActiveExpensesClicked ? activeExpensesNames
							: allActiveIncomesClicked ? activeIncomesNames
							: [""],
		}}
		options = {{
			legend: {
				labels: {
					fontColor: 'black'
				}
			}
		}}
		redraw = {false} />
		</>
	)
}