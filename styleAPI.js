import {makeStyles} from '@material-ui/core/styles'


export const useStyles = makeStyles((theme) => ({
  root: {
    background: "url('/background.jpg')",
    backgroundSize: "cover",
    backgroundPosition: "center",
    height: "100vh"
  },
  grids: {
    height: "80vh",
    maxHeight: "80vh",
    border: "none"
  },
  left: {
    borderBottomLeftRadius: "25px",
    borderTopLeftRadius: "25px",
    backgroundImage: "radial-gradient(circle, rgba(247,251,63,0.2) 0%, rgba(251,251,225,1) 2%, rgba(255,255,255,0.5) 5%, rgba(255,255,0,0.5) 8%, rgba(252,252,193,0.1) 10%, rgba(255,255,255,0.3) 37%, rgba(132,133,235,0.3) 42%, rgba(0,1,255,0.6472163865546218) 50%, rgba(255,255,255,1) 59%, rgba(129,137,226,0) 77%, rgba(105,105,249,0) 80%, rgba(255,255,255,0.5) 85%, rgba(126,128,251,0) 96%, rgba(18,22,238,0) 100%)"
  },
  right: {
    [theme.breakpoints.down('sm')]: {
    borderTopRightRadius: "0",
    borderBottomRightRadius: "0"
    },
    [theme.breakpoints.up('sm')]: {
    borderTopRightRadius: "25px",
    borderBottomRightRadius: "25px"
    },
    position: "relative",
    backgroundImage: "linear-gradient(90deg, rgba(245,245,245,0.9444152661064426) 0%, rgba(245,245,245,0.9662640056022409) 83%, rgba(245,245,245,0.96850490196078427) 100%)"
  },
  svgColor: {
    height: "70%",
    width: "80%"
  },
  form: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  textfield: {
    marginBottom: "calc(1rem - 5px)",
    [theme.breakpoints.up('lg')]: {
      width: "40ch"
    }
  },
  textfieldEmail: {
    [theme.breakpoints.up('lg')]: {
      width: "40ch"
    }
  },
  Alert: {
    position: "absolute",
    right: "0",
    top: "0",
    marginTop: "2rem"
  },
  textFieldHome: {
    display: "none"
  },
  rootFormRecord: {
      height: "100%",
      "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "90%",
      backgroundColor: "whitesmoke"
    }
  },
  rootPages: {
    height: "100vh",
    display: "flex",
    alignItems: "flex-start",
  }

  }))