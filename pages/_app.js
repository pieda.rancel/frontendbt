import {useState, useEffect} from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import {UserProvider} from '../UserContext'
import Head from 'next/head'
import Router from 'next/router'

function MyApp({ Component, pageProps }) {
 
 const [user, setUser] = useState({
 	id: null,
 	isAdmin: null,
 })

 const [balance, setBalance] = useState(0)
 const [records, setRecords] = useState([])

 const unsetUser = () => {
 	localStorage.clear()

 	setUser({
 		id: null,
 		isAdmin: null,
 		})

 	setBalance(0)
 	setRecords([])

 	Router.push('/')
 }




 useEffect(()=> {
 	fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
 		headers: {
 			Authorization: `Bearer ${localStorage.getItem('token')}`
 		}
 	})
 	.then(res => res.json())
 	.then(data => {
 		if(data._id){
 			const computeActiveBalance = () => {
 				const activeRecords = data.records.filter(record => record.isActive=== true)
 				return activeRecords.reduce((accumulatedValue, currentRecord) => {
 					if(currentRecord.type === "Expense"){
 						return +accumulatedValue - +currentRecord.amount
 					} else {
 						return +accumulatedValue + +currentRecord.amount
 					}
 				}, 0)
 			}


 			setUser({
 				id: data._id,
 				isAdmin: data.isAdmin,
 			})
 				setBalance(computeActiveBalance())
 				setRecords(data.records)
 		} else {
 			setUser({
 				id: null,
 				isAdmin: null
 			})
 			setBalance(0)
 			setRecords([])
 		}
 	})
 }, [user.id])

  return (
  	<UserProvider value={{user, setUser, unsetUser, balance, setBalance}}>
  		<Head>
  			<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
  			<link rel="shortcut icon" href="/icon.ico" />
 		</Head>
 		<Component {...pageProps} />	
  	</UserProvider>
  	)
}

export default MyApp
